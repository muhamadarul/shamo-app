class GalleryModel {
  int? id;
  String url = '';

  GalleryModel({this.id, required this.url});

  GalleryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? 0 ;
    url = json['url'] ??
        'https://images.unsplash.com/photo-1547721064-da6cfb341d50';
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'url': url,
    };
  }
}
