class UserModel {
  int? id;
  String? name;
  String? email;
  String? username;
  String profilePhotoUrl =
      'https://images.unsplash.com/photo-1547721064-da6cfb341d50';
  String? token;

  UserModel(
      {this.id,
      this.name,
      this.email,
      this.username,
      required this.profilePhotoUrl,
      this.token});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    email = json['email'];
    name = json['name'];
    profilePhotoUrl = json['image'];
    token = json['access_token'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'email': email,
      'username': username,
      'profile_photo_url': profilePhotoUrl,
      'token': token,
    };
  }
}
